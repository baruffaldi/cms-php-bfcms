<?php
/**
 * This is the guestbook form.  It is in its own directory in the application 
 * structure because it represents a "composite asset" in your application.  By 
 * "composite", it is meant that the form encompasses several aspects of the 
 * application: it handles part of the display logic (view), it also handles 
 * validation and filtering (controller and model).  
 */
class Form_UsersSignIn extends Zend_Dojo_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {
    	global $site;Zend_Dojo::enableForm($this);
        $this->setName('Registration form');
        // set the method for the display form to POST
        $this->setMethod('post');

        // add an email element
        $this->addElement('text', 'username', array(
            'label'      => 'Your username:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'Alnum',
            )
        ));
        $this->addElement('password', 'password', array(
            'label'      => 'Your password:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'Alnum',
            )
        ));
        $this->addElement('text', 'realname', array(
            'label'      => 'Your real name:',
            'required'   => true,
            'filters'    => array('StringTrim')/*,
            'validators' => array(
                array( 'validator' => 'StringLength', 'options' => array(0, 64))
                )*/
        ));
        $this->addElement('text', 'email', array(
            'label'      => 'Your email address:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
            )
        ));
        
        $this->addElement('text', 'country', array(
            'label'      => 'Your country:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'Alnum',
            )
        ));

        $this->addElement('captcha', 'captcha', array(
            'label'      => 'Please enter the 2 words displayed below:',
            'required'   => true,
            'captcha'    => array('captcha' => 'ReCaptcha', 'pubKey'  => $site['config']['handler']->site->pubKey, 
                                                            'privKey' => $site['config']['handler']->site->privKey )
        ));

        // add the submit button
        $this->addElement('submit', 'submit', array(
            'label'    => 'Sign In!',
        ));
    }
}