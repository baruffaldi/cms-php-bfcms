<?php

/**
 * Configure all environment settings
 */

// *** Debug Procedures
$site['config']['env']['type']  = $_SERVER['ENV'];
$site['config']['env']['debug'] = ( isset( $_GET['debug'] ) 
                                 || isset( $_POST['debug'] ) 
                                 || $_SERVER['ENV'] = 'development' 
                                ) ? TRUE : FALSE;

$site['config']['fs']['path_documentroot']  = dirname( __FILE__ )                        . '/../..';
$site['config']['fs']['path_lib']           = $site['config']['fs']['path_documentroot'] . '/library';
$site['config']['fs']['path_app']           = $site['config']['fs']['path_documentroot'] . '/application';
$site['config']['fs']['path_tpl']           = $site['config']['fs']['path_app']          . '/templates';
$site['config']['fs']['path_config']        = $site['config']['fs']['path_app']          . '/config';
$site['config']['fs']['path_log']           = $site['config']['fs']['path_app']          . '/logs';

// *** PHP Settings
set_include_path( get_include_path( ) . PATH_SEPARATOR 
                . $site['config']['fs']['path_lib'] );

if ( $site['config']['env']['debug'] ) error_reporting( E_ALL ^ E_NOTICE );
if ( $site['config']['env']['debug'] ) ini_set( 'display_errors', 1 );
else ini_set( 'display_errors', 0 );