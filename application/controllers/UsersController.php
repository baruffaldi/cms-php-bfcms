<?php

/**
 * UsersController - in this example, we will build a simple
 * Users style application.  It is capable only of being "signed" and
 * listing the previous entries.
 */
class UsersController extends Zend_Controller_Action 
{
    /** 
     * While overly simplistic, we will create a property for this controller 
     * to contain a reference to the model associated with this controller. In 
     * larger system, or in systems that might have referential models, we 
     * would need additional properties for each.
     */
    protected $_model;

    /**
     * The index, or landing, action will be concerned with listing the entries 
     * that already exist.
     *
     * Assuming the default route and default router, this action is dispatched 
     * via the following urls:
     *   /Users/
     *   /Users/index
     *
     * @return void
     */
    public function indexAction()
    {
        $model = $this->_getModel();
        $this->view->entries = $model->fetchEntries();
    }

    /**
     * _getModel() is a protected utility method for this controller. It is 
     * responsible for creating the model object and returning it to the 
     * calling action when needed. Depending on the depth and breadth of the 
     * application, this may or may not be the best way of handling the loading 
     * of models.  This concept will be visited in later tutorials, but for now 
     * - in this application - this is the best technique.
     *
     * Also note that since this is a protected method without the word 'Action',
     * it is impossible that the application can actually route a url to this 
     * method. 
     *
     * @return Model_Users
     */
    protected function _getModel()
    {
    	global $site;
    	
        if (null === $this->_model) {
            // autoload only handles "library" compoennts.  Since this is an 
            // application model, we need to require it from its application 
            // path location.
            require_once $site['config']['fs']['path_app'] . '/models/Users.php';
            $this->_model = new Model_Users();
        }
        return $this->_model;
    }

    /**
     * This method is essentially doing the same thing for the Form that we did 
     * above in the protected model accessor.  Same logic applies here.
     *
     * @return Form_Users
     */
    protected function _getUsersSignInForm()
    {
    	global $site;
    	
        require_once $site['config']['fs']['path_app'] . '/forms/UsersSignIn.php';
        $form = new Form_UsersSignIn(  );
        $form->setAction($this->_helper->url('sign'));
        return $form;
    }

    /**
     * This method is essentially doing the same thing for the Form that we did 
     * above in the protected model accessor.  Same logic applies here.
     *
     * @return Form_Users
     */
    protected function _getUsersLogInForm()
    {
    	global $site;
    	
        require_once $site['config']['fs']['path_app'] . '/forms/UsersLogin.php';
        $form = new Zend_Form( $site['config']['handler']->user->login );
        $form->setAction($this->_helper->url('sign'));
        return $form;
    }
    
    
    /**
     * The sign action is responsible for handling the "signing" of the 
     * guestbook. Since we are using default routes, this will be enacted when 
     * the url /guestbook/sign is visited.
     *
     * @return void
     */
    public function signAction()
    {
        $request = $this->getRequest();
        $form    = $this->_getUsersSignInForm();
        
        // check to see if this action has been POST'ed to
        if ($this->getRequest()->isPost()) {
            
            // now check to see if the form submitted exists, and
            // if the values passed in are valid for this form
            if ($form->isValid($request->getPost())) {
                
                // since we now know the form validated, we can now
                // start integrating that data sumitted via the form
                // into our model
                $model = $this->_getModel();
                $model->save($form->getValues());
                
                // now that we have saved our model, lets url redirect
                // to a new location
                // this is also considered a "redirect after post"
                // @see http://en.wikipedia.org/wiki/Post/Redirect/Get
                return $this->_helper->redirector('index');
            }
        }
        
        // assign the form to the view
        $this->view->form = $form;
    }
    
    
    /**
     * The sign action is responsible for handling the "signing" of the 
     * guestbook. Since we are using default routes, this will be enacted when 
     * the url /guestbook/sign is visited.
     *
     * @return void
     */
    public function logAction()
    {
        $request = $this->getRequest();
        $form    = $this->_getUsersLogInForm();
        
        // check to see if this action has been POST'ed to
        if ($this->getRequest()->isPost()) {
            
            // now check to see if the form submitted exists, and
            // if the values passed in are valid for this form
            if ($form->isValid($request->getPost())) {
                
                // since we now know the form validated, we can now
                // start integrating that data sumitted via the form
                // into our model
                $model = $this->_getModel();
                $model->save($form->getValues());
                
                // now that we have saved our model, lets url redirect
                // to a new location
                // this is also considered a "redirect after post"
                // @see http://en.wikipedia.org/wiki/Post/Redirect/Get
                return $this->_helper->redirector('index');
            }
        }
        
        // assign the form to the view
        $this->view->form = $form;
    }
}