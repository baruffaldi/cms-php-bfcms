<?php

/**
 * This model class represents the business logic associated with a "guestbook" 
 * model.  While its easy to say that models are generally derived from 
 * database tables, this is not always the case.  Data sources for models are 
 * commonly web services, the filesystem, caching systems, and more.  That 
 * said, for the purposes of this guestbook applicaiton, we have split the 
 * buisness logic from its datasource (the dbTable).
 *
 * This particular class follows the Table Module pattern.  There are other 
 * patterns you might want to employ when modeling for your application, but 
 * for the purposes of this example application, this is the best choice.
 * To understand different Modeling Paradigms: 
 * 
 * @see http://martinfowler.com/eaaCatalog/tableModule.html [Table Module]
 * @see http://martinfowler.com/eaaCatalog/ [See Domain Logic Patterns and Data Source Arch. Patterns]
 */

/**
 * This is the DbTable class for the guestbook table.
 */
class Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    /** Table name */
    protected $_name    = 'users';

    /**
     * Insert new row
     *
     * Ensure that a timestamp is set for the created field.
     * 
     * @param  array $data 
     * @return int
     */
    public function insert(array $data)
    {
        $data['creation_date'] = date('Y-m-d H:i:s');
        $data['rank'] = 0;
        $data['password'] = sha1( $data['password'] );
        
        return parent::insert($data);
    }

    /**
     * Override updating
     *
     * Do not allow updating of entries
     * 
     * @param  array $data 
     * @param  mixed $where 
     * @return void
     * @throws Exception
     */
    public function update(array $data, $where)
    {
        throw new Exception('Cannot update users entries');
    }
}