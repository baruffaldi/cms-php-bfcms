<?php

foreach( array_reverse( explode( ',', strtolower( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ) ) as $lang )
    if ( file_exists( $site['config']['fs']['path_app'] . "/languages/$lang.ini" ) ) 
        $site['language'] = parse_ini_file( $site['config']['fs']['path_app'] . "/languages/$lang.ini" );
        
foreach( $site['language'] as $key => $value )
    define( '__LANG_' . strtoupper( str_replace( '.', '_', $key ) ) . '__', $value );