<?php
/**
 * Enter description here...
 *
 * @version 0.1
 * @package StreamingAPI
 * @author Filippo Baruffaldi
 * @email filippo.baruffaldi@oneitalia.it
 * @last-revisioner Filippo Baruffaldi
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Adapter' . DIRECTORY_SEPARATOR . 'Exception.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Adapter' . DIRECTORY_SEPARATOR . 'Pdo' . DIRECTORY_SEPARATOR . 'Mysql.php';

$site['database']['handler']  = Zend_Db::factory( $site['config']['handler']->database );
$site['database']['tables']   = $site['database']['handler']->listTables( );
$site['database']['isActive'] = isset( $site['database']['tables'][0] );

$site['database']['handler']->setFetchMode( Zend_Db::FETCH_ASSOC );

Zend_Db_Table_Abstract::setDefaultAdapter( $site['database']['handler'] );