<?php

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Config.php';

// CONFIGURATION - Setup the configuration object
// The Zend_Config_Ini component will parse the ini file, and resolve all of
// the values for the given section.  Here we will be using the section name
// that corresponds to the APP's Environment
$site['config']['handler'] = new Zend_Config_Ini(
    $site['config']['fs']['path_app'] . '/config/app.ini', 
    $site['config']['env']['type'] );
