<?php
//
require_once 'Zend' . DIRECTORY_SEPARATOR . '/Loader.php';

/**
 * Instantiate all objects
 */
Zend_Loader::registerAutoload();

// Step 2: FRONT CONTROLLER - Get the front controller.
// The Zend_Front_Controller class implements the Singleton pattern, which is a
// design pattern used to ensure there is only one instance of
// Zend_Front_Controller created on each request.
$site['frontController']['handler'] = Zend_Controller_Front::getInstance();
// Step 3: CONTROLLER DIRECTORY SETUP - Point the front controller to your action
// controller directory.
$site['frontController']['handler']->setControllerDirectory( $site['config']['fs']['path_app'] . '/controllers' );

// Step 4: APPLICATION ENVIRONMENT - Set the current environment.
// Set a variable in the front controller indicating the current environment --
// commonly one of development, staging, testing, production, but wholly
// dependent on your organization's and/or site's needs.
$site['frontController']['handler']->setParam( 'env', $site['config']['env']['type'] );


