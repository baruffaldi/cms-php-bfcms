<?php
// LAYOUT SETUP - Setup the layout component
// The Zend_Layout component implements a composite (or two-step-view) pattern
// With this call we are telling the component where to find the layouts scripts.
Zend_Layout::startMvc( $site['config']['fs']['path_app'] . '/layouts/scripts' );

// VIEW SETUP - Initialize properties of the view object
// The Zend_View component is used for rendering views. Here, we grab a "global" 
// view instance from the layout object, and specify the doctype we wish to 
// use. In this case, XHTML1 Strict.
$site['layout']['handler'] = Zend_Layout::getMvcInstance()->getView();
$site['layout']['handler']->doctype( 'XHTML1_STRICT' );