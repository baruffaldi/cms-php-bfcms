<?php


// REGISTRY - setup the application registry
// An application registry allows the application to store application 
// necessary objects into a safe and consistent (non global) place for future 
// retrieval.  This allows the application to ensure that regardless of what 
// happends in the global scope, the registry will contain the objects it 
// needs.
$site['registry']['handler'] = Zend_Registry::getInstance();

$site['registry']['handler']->configuration = $site['config']['handler'];

if ( is_resource( $site['database']['handler'] ) )
    $site['registry']['handler']->dbAdapter     = $site['database']['handler'];