<?php
/**
 * Include all needed files
 */

// MVC Handlers
require_once $site['config']['fs']['path_app'] . '/init/loader.php';

// MVC Handlers
require_once $site['config']['fs']['path_app'] . '/init/layout.php';

// Configuration Handler
require_once $site['config']['fs']['path_app'] . '/init/config.php';

foreach ( $site['config']['handler']->init as $initModule )
    if ( file_exists( $site['config']['fs']['path_app'] . "/init/$initModule.php" ) ) 
        require_once $site['config']['fs']['path_app'] . "/init/$initModule.php";

// CLEANUP - remove items from global scope
// This will clear all our local boostrap variables from the global scope of 
// this script (and any scripts that called bootstrap).  This will enforce 
// object retrieval through the Applications's Registry
//unset( $initModule );
//unset( $site );