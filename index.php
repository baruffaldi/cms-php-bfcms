<?
/**
 * Index
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 */

require_once dirname( __FILE__ ) . '/application/config/environment.php';

try { require $site['config']['fs']['path_app'] . '/bootstrap.php'; }
 
catch ( Exception $exception ) {

/**
 * @todo write a smarty template to get this courtesy message
 */
    echo '<html><body><center>'
       . 'An exception occured while bootstrapping the application.';
       
    if ($site['config']['env']['type'] != 'production' ) 
        echo '<br /><br />' . $exception->getMessage( ) . '<br />'
           . '<div align="left">Stack Trace:' 
           . '<pre>' . $exception->getTraceAsString( ) . '</pre></div>';
    
    echo '</center></body></html>';
    exit( 1 );
}

// *** Let's output!
//$site['frontController']['handler']->addControllerDirectory($site['config']['fs']['path_app'] . '/controllers');
$site['frontController']['handler']->dispatch();
print "EOF! :)";

//_dump($_SERVER);
unset( $site );